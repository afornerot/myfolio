#!/bin/bash

# Se positionner sur la racine du projet
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${DIR}
DIR=$(pwd)

echo Mise à jour
git reset --hard origin/master
git pull
echo ""
echo Reconfiguration
scripts/reconfigure.sh
echo ""