<?php

namespace App\Repository;

use App\Entity\Cron;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CronRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cron::class);
    }

    public function toExec()
    {
        // Les commandes à executer
        // = statut = 0 (à executer)
        // = statut = 2 (OK) et derniere execution + interval > now et nombre d'appel = 0
        // = statut = 3 (KO) et derniere execution + interval > now et nombre d'appel = 0
        // = statut = 3 (KO) et nombre d'execution < nombre d'appel
        

        $now=new \DateTime();

        $qb = $this->createQueryBuilder('cron')
            ->Where('cron.statut=0')
            ->orWhere('cron.statut=2 AND cron.nextexecdate<:now AND cron.repeatcall=0')
            ->orWhere('cron.statut=3 AND cron.nextexecdate<:now AND cron.repeatcall=0') 
            ->orWhere('cron.statut=3 AND cron.nextexecdate<:now AND cron.repeatcall>cron.repeatexec');

        return $qb->getQuery()->setParameter('now',$now->format("Y-m-d H:i:s"))->getResult();  
    }          
}
