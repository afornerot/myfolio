<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

class CronType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('submit', SubmitType::class, [
                "label"       => "Valider",
                "attr"        => array("class" => "btn btn-success")
            ])
                
            ->add('command', TextType::class, [
                'label'         => 'Commande',
                "disabled"      => true,
            ])

            ->add('jsonargument', TextType::class, [
                'label'         => 'Argument Commande au format json',
                "disabled"      => true,
            ])                                                  

            ->add('statut', ChoiceType::class, [
                'label' => "Statut",
                'choices' => array("A éxécuter" => "0","Exécution en cours" => "1","OK" => "2","KO" => "3","Désactivé" => "4")
            ])

            ->add('repeatcall', IntegerType::class, [
                'label' => "Nombre d'éxécution en cas d'echec. Si zéro on le répete à l'infini même si OK"
            ])

            ->add('repeatinterval', IntegerType::class, [
                'label' => "Interval en seconde entre deux éxécution"
            ])
            
            ->add('nextexecdate', DatetimeType::class, [
                'label' => "Prochaine exécution"
            ])
            ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Cron',
            'mode'          => 'string'
        ]);
    }
}
