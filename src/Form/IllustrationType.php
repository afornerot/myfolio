<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;


class IllustrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );
       
        $builder->add('name',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('description',
            CKEditorType::class, [
                "required" => false,
                "config" => [
                    'uiColor' => '#ffffff',
                    'height' => 600,
                    'filebrowserUploadRoute' => 'app_ckeditor_upload',
                    'language' => 'fr',
                ],
            ]
        );   
        
        $builder->add('category',
            EntityType::class, [
                "class"       => "App:Category",
                "label"       => "Categorie",
                "choice_label"=> "name",
            ]
        );
                  
        $builder->add('illustration',HiddenType::class);
        $builder->add('width',HiddenType::class);
        $builder->add('height',HiddenType::class);                  
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Illustration',
            'mode'          => 'string',
        ));
    }
}
