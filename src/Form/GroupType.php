<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class GroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );

        $builder->add('name',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('users',
            Select2EntityType::class, [
                "label" => "Utilisateurs",
                "disabled" => false,
                "required"    => false,
                "multiple" => true,
                "remote_route" => "app_user_select",
                "class" => "App:User",
                "primary_key" => "id",
                "text_property" => "displayname",
                "minimum_input_length" => 0,
                "page_limit" => 100,
                "allow_clear" => true,
                "delay" => 250,
                "cache" => false,
                "cache_timeout" => 60000,
                "language" => "fr",
                "placeholder" => "Selectionner des Utilisateurs",
            ]
        );         
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Group',
            'mode'          => 'string',
        ));
    }
}
