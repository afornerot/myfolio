<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit',
            SubmitType::class,
            array("label"       => "Valider",
                  "attr"        => array("class" => "btn btn-success")));

        $builder->add('id',
            TextType::class,
            array("label"       =>"Clé",
                  "label_attr"  => array("style" => 'margin-top:15px;'),
                  "attr"        => array("class" => "form-control"),
                  'disabled'    => true));

        switch($options["type"]) {
            case "string":
                $builder->add('value',
                    TextType::class,
                    array("label"       => "Valeur",
                          "label_attr"  => array("style" => 'margin-top:15px;'),
                          "attr"        => array("class" => "form-control"),
                          'required'    => ($options["required"]==0?false:true)));
                break;

            case "boolean":
                $choices=["oui" => "1","non" => "0"];
                $builder->add("value", ChoiceType::class,
                    array("label"       =>"Valeur",
                          "label_attr"  => array("style" => 'margin-top:15px;'),
                          "attr"        => array("class" => "form-control"),
                          'required'    => ($options["required"]==0?false:true),
                          "choices"     => $choices));
                break;

            case "integer":
                $builder->add("value",
                    IntegerType::class, [
                        "label"       =>"Valeur",
                        "label_attr"  => array("style" => 'margin-top:15px;'),
                        "attr"        => array("class" => "form-control","min" => "0"),
                        "required"    => ($options["required"]==0?false:true),
                    ]
                );
            break;

            case "pourcentage":
                $builder->add("value",
                    IntegerType::class, [
                        "label"       =>"Valeur",
                        "label_attr"  => array("style" => 'margin-top:15px;'),
                        "attr"        => array("class" => "form-control","min" => "0", "max"=>"100"),
                        "required"    => ($options["required"]==0?false:true),
                    ]
                );
            break;

            case "thumbwidth":
                $choices=["variable" => 0,"10%" => 1,"20%" => 2];
                $builder->add("value",
                    ChoiceType::class, [
                        "label"       =>"Valeur",
                        "label_attr"  => array("style" => 'margin-top:15px;'),
                        "attr"        => array("class" => "form-control"),
                        "required"    => ($options["required"]==0?false:true),
                        "choices"     => $choices
                    ]
                    );
            break;

            case "thumbheight":
                $choices=["carré" => 0,"proportionnelle" => 1,"page" => 2];
                $builder->add("value",
                    ChoiceType::class, [
                        "label"       =>"Valeur",
                        "label_attr"  => array("style" => 'margin-top:15px;'),
                        "attr"        => array("class" => "form-control"),
                        "required"    => ($options["required"]==0?false:true),
                        "choices"     => $choices
                    ]
                    );
            break;            

            case "font":
                $choices=[
                    "ABeeZee-Regular" => "ABeeZee-Regular",
                    "Acme-Regular" => "Acme-Regular",
                    "AlfaSlabOne-Regular" => "AlfaSlabOne-Regular",
                    "Anton-Regular" => "Anton-Regular",
                    "Baloo-Regular" => "Baloo-Regular",
                    "CarterOne-Regular" => "CarterOne-Regular",
                    "Chewy-Regular" => "Chewy-Regular",
                    "Courgette-Regular" => "Courgette-Regular",
                    "FredokaOne-Regular" => "FredokaOne-Regular",
                    "Grandstander" => "Grandstander",
                    "Helvetica" => "Helvetica",
                    "Justanotherhand-Regular" => "Justanotherhand-Regular",
                    "Lato-Regular" => "Lato-Regular",
                    "LexendDeca-Regular" => "LexendDeca-Regular",
                    "LuckiestGuy-Regular" => "LuckiestGuy-Regular",
                    "Overpass-Black" => "Overpass-Black",
                    "PassionOne" => "PassionOne",
                    "Peacesans" => "Peacesans",
                    "Redressed" => "Redressed",
                    "Righteous-Regular" => "Righteous-Regular",
                    "Roboto-Regular" => "Roboto-Regular",
                    "RubikMonoOne-Regular" => "RubikMonoOne-Regular",
                    "SigmarOne-Regular" => "SigmarOne-Regular",
                    "Signika-Regular" => "Signika-Regular",
                    "Teko-Bold" => "Teko-Bold",
                    "Viga-Regular" => "Viga-Regular",
                ];

                $builder->add("value", ChoiceType::class,
                    array("label"       =>"Valeur",
                          "label_attr"  => array("style" => 'margin-top:15px;'),
                          "attr"        => array("class" => "form-control"),
                          'required'    => ($options["required"]==0?false:true),
                          "choices"     => $choices));
                break;
           
            case "editor":
                $builder->add('value',
                    CKEditorType::class,[
                        "required" => ($options["required"]==0?false:true),
                        "config" => [
                            'uiColor' => '#ffffff',
                            'height' => 600,
                            'filebrowserUploadRoute' => 'app_ckeditor_upload',
                            'language' => 'fr',
                        ]
                    ]
                );
            break;

            case "logo":
                $builder->add('value',HiddenType::class);
                break;

            case "hero":
                $builder->add('value',HiddenType::class);
                break;

            case "image":
                $builder->add('value',HiddenType::class);
                break;

            case "color":
                $builder->add('value',
                    TextType::class,
                    array("label"       => "Valeur",
                          "label_attr"  => array("style" => 'margin-top:15px;'),
                          "attr"        => array("class" => "pick-a-color form-control"),
                          'required'    => ($options["required"]==0?false:true)));
                break;
        }

        $builder->add('help',
            TextareaType::class,
            array("label"       =>"Aide",
                  "attr"        => array("class" => "form-control", "style" => "margin-top:15px; height: 200px;"),
                  'required'    => false,
                  'disabled'    => true));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Config',
            'mode'          => "string",
            'id'            => "string",
            'type'          => "string",
            'required'      => "string",
        ));
    }
}
