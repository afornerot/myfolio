<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $boolean=["non" => "0","oui" => "1"];
        $thumbwidth=["variable" => 0,"10%" => 1,"20%" => 2];
        $thumbheight=["carré" => 0,"proportionnelle" => 1,"page"=>2];

        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );

        $builder->add('order',
            IntegerType::class, [
                "label" =>"Ordre",
            ]
        );
        
        $builder->add('name',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('usecategoryconfig',
            ChoiceType::class, [
                "label" =>"Utiliser une configuration de style spécifique",
                "choices" => $boolean,
            ]
        );
         
        $builder->add('appthumbwidth', 
            ChoiceType::class, [
                "label" =>"Largeur des miniatures",
                "choices" => $thumbwidth,
            ]
        );

        $builder->add('appthumbheight', 
            ChoiceType::class, [
                "label" =>"Hauteur des miniatures",
                "choices" => $thumbheight,
            ]
        );

        $builder->add('appthumbfilter',
            ChoiceType::class, [
                "label" =>"Appliquer un filtre CSS sur les miniatures",
                "choices" => $boolean,
            ]
        );

        $builder->add('appthumbfiltergrayscale',
            IntegerType::class, [
                "label"       =>"Filtre grayscale",
                "attr"        => ["min" => "0", "max"=>"100"],
            ]
        );

        $builder->add('appthumbfilteropacity',
            IntegerType::class, [
                "label"       =>"Filtre opacity",
                "attr"        => ["min" => "0", "max"=>"100"],
            ]
        );

        $builder->add('appthumbfiltersepia',
            IntegerType::class, [
                "label"       =>"Filtre sepia",
                "attr"        => ["min" => "0", "max"=>"100"],
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Category',
            'mode'          => 'string',
        ));
    }
}
