<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );

        $builder->add('order',
            IntegerType::class, [
                "label" =>"Ordre",
            ]
        );
        
        $builder->add('name',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('url',
            TextType::class, [
                "label" =>"URL",
            ]
        );        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Link',
            'mode'          => 'string',
        ));
    }
}
