<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('avatar',HiddenType::class, array("empty_data" => "noavatar.png"));

        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );

        $builder->add('username',
            TextType::class, [
                "label" =>"Login",
                "disabled" => ($options["mode"]!="submit"),
                "attr" => ["autocomplete" => "new-password"],
            ]
        );

        if($options["mode"]!="profil") {
            $choices=[];
            $choices['Administrateur']='ROLE_ADMIN';
            $choices['Modérateur']='ROLE_MODO';
            $choices['Master']='ROLE_MASTER';
            $choices['Utilisateur']='ROLE_USER';

            $builder->add('roles', 
                ChoiceType::class, [
                    'choices'   => $choices,
                    'multiple'  => true,
                    'expanded'  => true,
                ]
            );

            $builder->add('groups',
                Select2EntityType::class, [
                    "label" => "Groupes",
                    "disabled" => false,
                    "required"    => false,
                    "multiple" => true,
                    "remote_route" => "app_group_select",
                    "class" => "App:Group",
                    "primary_key" => "id",
                    "text_property" => "name",
                    "minimum_input_length" => 0,
                    "page_limit" => 100,
                    "allow_clear" => true,
                    "delay" => 250,
                    "cache" => false,
                    "cache_timeout" => 60000,
                    "language" => "fr",
                    "placeholder" => "Selectionner des Groupes",
                ]
            );        
        }
        
        $builder->add('lastname',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('firstname',
            TextType::class, [
                "label" =>"Prénom",
                "required" => false,
            ]
        );

        $builder->add('email',
            EmailType::class, [
                "label" =>"Email",
            ]
        );

        $builder->add('password',
            RepeatedType::class, [
                "type" => PasswordType::class,
                "required" => ($options["mode"]=="submit"?true:false),
                "options" => array("always_empty" => true),
                "first_options"  => array("label" => "Mot de Passe","attr" => array("class" => "form-control", "style" => "margin-bottom:15px", "autocomplete" => "new-password")),
                "second_options" => array('label' => 'Confirmer Mot de Passe',"attr" => array("class" => "form-control", "style" => "margin-bottom:15px"))
            ]
        );        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\User',
            'mode'          => 'string',
            'appAuth'       => 'string',
        ));
    }
}
