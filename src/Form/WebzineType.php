<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;


class WebzineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('submit',
            SubmitType::class, [
                "label" => "Valider",
                "attr" => ["class" => "btn btn-success no-print"],
            ]
        );
       
        $builder->add('name',
            TextType::class, [
                "label" =>"Nom",
            ]
        );

        $builder->add('set',
            TextType::class, [
                "label" =>"Série",
                "required" => false,
            ]
        );

        $builder->add('order',
            IntegerType::class, [
                "label" =>"Ordre",
            ]
        );

        $builder->add('mode',
            ChoiceType::class, [
                "label" =>"Mode de visualisation des planches",
                "choices" => ["Planche par Planche"=>0],
            ]
        );


        $builder->add('description',
            CKEditorType::class, [
                "required" => false,
                "config" => [
                    'uiColor' => '#ffffff',
                    'height' => 600,
                    'filebrowserUploadRoute' => 'app_ckeditor_upload',
                    'language' => 'fr',
                ],
            ]
        );   

        $builder->add('linkpages',HiddenType::class, array("mapped" => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'App\Entity\Webzine',
            'mode'          => 'string',
        ));
    }
}
