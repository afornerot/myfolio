<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Webzine
 *
 * @ORM\Table(name="webzine")
 * @ORM\Entity(repositoryClass="App\Repository\WebzineRepository")
 */
class Webzine
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $submittime;

    /**
     * @ORM\Column(name="setname", type="string", length=100, nullable=true)
     */
    private $set;

    /**
     * @ORM\Column(name="roworder", type="integer", nullable=true)
     */
    private $order;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $mode;
    
    /**
     * @ORM\OneToMany(targetEntity="Webzinepage", mappedBy="webzine", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $webzinepages;

    public function __construct()
    {
        $this->webzinepages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSubmittime(): ?\DateTimeInterface
    {
        return $this->submittime;
    }

    public function setSubmittime(\DateTimeInterface $submittime): self
    {
        $this->submittime = $submittime;

        return $this;
    }

    public function getSet(): ?string
    {
        return $this->set;
    }

    public function setSet(?string $set): self
    {
        $this->set = $set;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getMode(): ?int
    {
        return $this->mode;
    }

    public function setMode(int $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * @return Collection|Webzinepage[]
     */
    public function getWebzinepages(): Collection
    {
        return $this->webzinepages;
    }

    public function addWebzinepage(Webzinepage $webzinepage): self
    {
        if (!$this->webzinepages->contains($webzinepage)) {
            $this->webzinepages[] = $webzinepage;
            $webzinepage->setWebzine($this);
        }

        return $this;
    }

    public function removeWebzinepage(Webzinepage $webzinepage): self
    {
        if ($this->webzinepages->removeElement($webzinepage)) {
            // set the owning side to null (unless already changed)
            if ($webzinepage->getWebzine() === $this) {
                $webzinepage->setWebzine(null);
            }
        }

        return $this;
    }

}
