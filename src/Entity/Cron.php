<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cron
 *
 * @ORM\Table(name="cron")
 * @ORM\Entity(repositoryClass="App\Repository\CronRepository")
 */
class Cron
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="command", type="string", nullable=false)
     * @Assert\NotBlank()
     *
     */
    private $command;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;  

    /**
     * @ORM\Column(type="integer")
     */
    private $statut;  

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $submitdate;    

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startexecdate;    

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endexecdate;    

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $nextexecdate;        

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repeatcall;   

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repeatexec;  

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repeatinterval;  

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $jsonargument; 

    private $statutlabel;

    // A garder pour forcer l'id en init
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    // A garder pour récupérer le label du statut
    public function getStatutlabel()
    {
        switch($this->statut) {
            case 0: $this->statutlabel="A éxécuter"; break;
            case 1: $this->statutlabel="Exécution en cours"; break;
            case 2: $this->statutlabel="OK"; break;
            case 3: $this->statutlabel="KO"; break;
        }
        return $this->statutlabel;
    }

    public function __construct()
    {
        $this->submitdate = new \DateTime(); 
    }   
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set command
     *
     * @param string $command
     *
     * @return Cron
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Cron
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set statut
     *
     * @param integer $statut
     *
     * @return Cron
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return integer
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set submitdate
     *
     * @param \DateTime $submitdate
     *
     * @return Cron
     */
    public function setSubmitdate($submitdate)
    {
        $this->submitdate = $submitdate;

        return $this;
    }

    /**
     * Get submitdate
     *
     * @return \DateTime
     */
    public function getSubmitdate()
    {
        return $this->submitdate;
    }

    /**
     * Set startexecdate
     *
     * @param \DateTime $startexecdate
     *
     * @return Cron
     */
    public function setStartexecdate($startexecdate)
    {
        $this->startexecdate = $startexecdate;

        return $this;
    }

    /**
     * Get startexecdate
     *
     * @return \DateTime
     */
    public function getStartexecdate()
    {
        return $this->startexecdate;
    }

    /**
     * Set endexecdate
     *
     * @param \DateTime $endexecdate
     *
     * @return Cron
     */
    public function setEndexecdate($endexecdate)
    {
        $this->endexecdate = $endexecdate;

        return $this;
    }

    /**
     * Get endexecdate
     *
     * @return \DateTime
     */
    public function getEndexecdate()
    {
        return $this->endexecdate;
    }

    /**
     * Set nextexecdate
     *
     * @param \DateTime $nextexecdate
     *
     * @return Cron
     */
    public function setNextexecdate($nextexecdate)
    {
        $this->nextexecdate = $nextexecdate;

        return $this;
    }

    /**
     * Get nextexecdate
     *
     * @return \DateTime
     */
    public function getNextexecdate()
    {
        return $this->nextexecdate;
    }

    /**
     * Set repeatcall
     *
     * @param integer $repeatcall
     *
     * @return Cron
     */
    public function setRepeatcall($repeatcall)
    {
        $this->repeatcall = $repeatcall;

        return $this;
    }

    /**
     * Get repeatcall
     *
     * @return integer
     */
    public function getRepeatcall()
    {
        return $this->repeatcall;
    }

    /**
     * Set repeatexec
     *
     * @param integer $repeatexec
     *
     * @return Cron
     */
    public function setRepeatexec($repeatexec)
    {
        $this->repeatexec = $repeatexec;

        return $this;
    }

    /**
     * Get repeatexec
     *
     * @return integer
     */
    public function getRepeatexec()
    {
        return $this->repeatexec;
    }

    /**
     * Set repeatinterval
     *
     * @param integer $repeatinterval
     *
     * @return Cron
     */
    public function setRepeatinterval($repeatinterval)
    {
        $this->repeatinterval = $repeatinterval;

        return $this;
    }

    /**
     * Get repeatinterval
     *
     * @return integer
     */
    public function getRepeatinterval()
    {
        return $this->repeatinterval;
    }

    /**
     * Set jsonargument
     *
     * @param string $jsonargument
     *
     * @return Cron
     */
    public function setJsonargument($jsonargument)
    {
        $this->jsonargument = $jsonargument;

        return $this;
    }

    /**
     * Get jsonargument
     *
     * @return string
     */
    public function getJsonargument()
    {
        return $this->jsonargument;
    }
}
