<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(name="roworder", type="integer", nullable=true)
     */
    private $order;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $usecategoryconfig;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appthumbwidth;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appthumbheight;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $appthumbfilter;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appthumbfiltergrayscale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appthumbfilteropacity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appthumbfiltersepia;

    /**
     * @ORM\OneToMany(targetEntity="Illustration", mappedBy="category", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $illustrations;

    public function __construct()
    {
        $this->illustrations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getUsecategoryconfig(): ?bool
    {
        return $this->usecategoryconfig;
    }

    public function setUsecategoryconfig(?bool $usecategoryconfig): self
    {
        $this->usecategoryconfig = $usecategoryconfig;

        return $this;
    }

    public function getAppthumbwidth(): ?int
    {
        return $this->appthumbwidth;
    }

    public function setAppthumbwidth(?int $appthumbwidth): self
    {
        $this->appthumbwidth = $appthumbwidth;

        return $this;
    }

    public function getAppthumbheight(): ?int
    {
        return $this->appthumbheight;
    }

    public function setAppthumbheight(?int $appthumbheight): self
    {
        $this->appthumbheight = $appthumbheight;

        return $this;
    }

    public function getAppthumbfilter(): ?bool
    {
        return $this->appthumbfilter;
    }

    public function setAppthumbfilter(?bool $appthumbfilter): self
    {
        $this->appthumbfilter = $appthumbfilter;

        return $this;
    }

    public function getAppthumbfiltergrayscale(): ?int
    {
        return $this->appthumbfiltergrayscale;
    }

    public function setAppthumbfiltergrayscale(?int $appthumbfiltergrayscale): self
    {
        $this->appthumbfiltergrayscale = $appthumbfiltergrayscale;

        return $this;
    }

    public function getAppthumbfilteropacity(): ?int
    {
        return $this->appthumbfilteropacity;
    }

    public function setAppthumbfilteropacity(?int $appthumbfilteropacity): self
    {
        $this->appthumbfilteropacity = $appthumbfilteropacity;

        return $this;
    }

    public function getAppthumbfiltersepia(): ?int
    {
        return $this->appthumbfiltersepia;
    }

    public function setAppthumbfiltersepia(?int $appthumbfiltersepia): self
    {
        $this->appthumbfiltersepia = $appthumbfiltersepia;

        return $this;
    }

    /**
     * @return Collection|Illustration[]
     */
    public function getIllustrations(): Collection
    {
        return $this->illustrations;
    }

    public function addIllustration(Illustration $illustration): self
    {
        if (!$this->illustrations->contains($illustration)) {
            $this->illustrations[] = $illustration;
            $illustration->setCategory($this);
        }

        return $this;
    }

    public function removeIllustration(Illustration $illustration): self
    {
        if ($this->illustrations->removeElement($illustration)) {
            // set the owning side to null (unless already changed)
            if ($illustration->getCategory() === $this) {
                $illustration->setCategory(null);
            }
        }

        return $this;
    }

}
