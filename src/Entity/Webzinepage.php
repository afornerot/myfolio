<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Illustration
 *
 * @ORM\Table(name="webzinepage")
 * @ORM\Entity(repositoryClass="App\Repository\WebzinepageRepository")
 */
class Webzinepage
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="roworder", type="integer", nullable=true)
     */
    private $order;

    /**
     * @ORM\Column(type="integer")
     */
    private $width;

    /**
     * @ORM\Column(type="integer")
     */
    private $height;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $illustration;

    /**
     * @ORM\ManyToOne(targetEntity="Webzine")
     */
    private $webzine;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getWebzine(): ?Webzine
    {
        return $this->webzine;
    }

    public function setWebzine(?Webzine $webzine): self
    {
        $this->webzine = $webzine;

        return $this;
    }
}
