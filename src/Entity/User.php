<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * User
 * 
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user",uniqueConstraints={@ORM\UniqueConstraint(name="username", columns={"username"})})
 * @UniqueEntity("username", message="Ce nom d'utilisateur existe dèja")
 */

class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array", length=255)
     */
    private $roles = array();

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt = '';

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $lastname;
    private $displayname;

    /**
      * @ORM\Column(type="string", length=200, nullable=true, options={"default" : 0})
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="usergroupe",
     *   joinColumns={@ORM\JoinColumn(name="user", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="groupe", referencedColumnName="id")}
     * )
     */
    private $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->surveys = new ArrayCollection();
        $this->guests = new ArrayCollection();
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setPassword($password): self
    {
        if($password!=$this->password&&$password!=""&&!is_null($password)){
            $this->salt = uniqid(mt_rand(), true);
            $hash = "{SSHA}" . base64_encode(pack("H*", sha1($password . $this->salt)) . $this->salt);

            $this->password = $hash;
        }

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function hasRole(string $role): ?bool
    {
        return in_array($role,$this->roles);
    }

    public function eraseCredentials()
    {
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt
        ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function getDisplayname()
    {
        return $this->firstname." ".$this->lastname;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }


    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAvatar(): ?string
    {
        if($this->avatar)
            return $this->avatar;
        else
            return "noavatar.png";
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }
}
