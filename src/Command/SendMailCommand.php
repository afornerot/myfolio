<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class SendMailCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:sendMail')
            ->setDescription('Envoi des mails')
            ->setHelp('Envoi des mails')
            ->addArgument('message-limit', InputArgument::OPTIONAL, 'limit message Mail') 
            ->addArgument('env', InputArgument::OPTIONAL, 'env Mail') 
            ->addArgument('cronid', InputArgument::OPTIONAL, 'ID Cron Job') 
            ->addArgument('lastchance', InputArgument::OPTIONAL, 'Lastchance to run the cron')             
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";

        $this->writelnred('');
        $this->writelnred('== app:sendMail');        
        $this->writelnred('==========================================================================================================');

        $appMailmethod=$this->container->getParameter("appMailmethod");

        $command = $this->getApplication()->find("swiftmailer:spool:send");
        $tbparameter["--message-limit"]=100;
        $tbparameter["--env"]="prod";
        $tbparameter["--time-limit"]=5;
        if($appMailmethod=="sendmail") $tbparameter["--transport"]="app.sendmail.transport";
        

        $parameter = new ArrayInput($tbparameter);
        try{
            $returnCode = $command->run($parameter, $output);
        }
        catch(\Exception $e) { 
            $this->writeln(""); 
            $this->writelnred("Impossible d'envoyer des mails"); 
            $this->writeln(""); 
            return 0;
        }
        $this->writeln(""); 
        
        return 1;

    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }   

}
