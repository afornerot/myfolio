<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Finder\Finder;

class PurgeFileCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    private $byexec;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:purgeFile')
            ->setDescription('Suppression des fichier obsolètes')
            ->setHelp('Suppression des fichier obsolètes')
            ->addArgument('cronid', InputArgument::OPTIONAL, 'ID Cron Job') 
            ->addArgument('lastchance', InputArgument::OPTIONAL, 'Lastchance to run the cron')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";
        $alias              = $this->container->getParameter('appAlias');

        $this->writelnred('');
        $this->writelnred('== app:purgeFile');        
        $this->writelnred('==========================================================================================================');

        $now=new \DateTime('now');

        $directory=$this->container->get('kernel')->getProjectDir()."/public/uploads/avatar";
        $this->writeln($directory);

        $files=[];
        $fs = new Filesystem();

        if($fs->exists($directory)) {
            $finder = new Finder();
            $finder->in($directory)->files();

            foreach (iterator_to_array($finder) as $file) {
                $name   = $file->getRelativePathname();
                if($name!="admin.jpg"&&$name!="noavatar.png"&&$name!="system.jpg") {
                    $entity=$this->em->getRepository("App:User")->findBy(["avatar"=>$name]);
                    if(!$entity) { 
                        $this->writeln($name);
                        $url=$directory."/".$name;
                        if($fs->exists($url)) {
                            $fs->remove($url);
                        }                          
                    }
                }
            }
        }

        $fs = new Filesystem();
        $users=$this->em->getRepository("App:User")->findAll();
        foreach($users as $user) {
            if(!$fs->exists($directory."/".$user->getAvatar())) {
                $this->writeln($user->getUsername());
                $user->setAvatar("noavatar.png");
                $this->em->persist($user);
                $this->em->flush();                
            }
        }

        $this->writeln('');
        return 1;
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }     
}
