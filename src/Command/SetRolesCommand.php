<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class SetRolesCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:setRoles')
            ->setDescription("Modifier le rôle d'un utilisateur")
            ->setHelp("Modifier le rôle d'un utilisateur")
            ->addArgument('username', InputArgument::OPTIONAL, 'username')            
            ->addArgument('roles', InputArgument::OPTIONAL, 'roles')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";


        $this->writelnred('');
        $this->writelnred('== app:setRoles');        
        $this->writelnred('==========================================================================================================');

        $username = $input->getArgument('username');
        $this->writeln($username);
        
        $this->writeln($input->getArgument('roles'));
        $roles = explode(",",$input->getArgument('roles'));
        //$this->writeln($roles);

        $user = $this->em->getRepository('App:User')->findOneBy(array('username' => $username));
        if($user) {
            // Set Password
            $user->setRoles($roles);
            $this->em->persist($user);
            $this->em->flush();               
        }

        $this->writeln('');
        return 1;
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }   



}
