<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Finder\Finder;

class DumpBddCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    private $byexec;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:dumpBdd')
            ->setDescription('Sauvegarde de la BDD')
            ->setHelp('Sauvegarde de la BDD')
            ->addArgument('cronid', InputArgument::OPTIONAL, 'ID Cron Job') 
            ->addArgument('lastchance', InputArgument::OPTIONAL, 'Lastchance to run the cron')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";
        $alias              = $this->container->getParameter('appAlias');

        $this->writelnred('');
        $this->writelnred('== app:dumpBdd');        
        $this->writelnred('==========================================================================================================');
        
        $this->datahost = $this->container->getParameter('databaseHost');
        $this->database = $this->container->getParameter('databaseName') ; 
        $this->username = $this->container->getParameter('databaseUser') ; 
        $this->password = $this->container->getParameter('databasePassword') ; 
    
        $cmd = sprintf('mysqldump -h %s -B %s -u %s --password=%s'
            , $this->datahost
            , $this->database
            , $this->username
            , $this->password
        );

        $result = $this->runCommand($cmd);
        if($result['exit_status'] == 0) {
            $this->filesystem->dumpFile($this->rootlog.$alias.".sql", implode('', $result['output']));
        }
        
        $this->writeln('');
        return 1;
    }

    protected function runCommand($command)
    {
        $command .=" >&1";
        exec($command, $output, $exit_status);
        return array(
              "output"      => $output
            , "exit_status" => $exit_status
        );
    }    

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }  
}
