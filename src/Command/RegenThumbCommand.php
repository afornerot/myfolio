<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Finder\Finder;

class RegenThumbCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    private $byexec;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:regenthumb')
            ->setDescription('Regénère les thumbs des illustrations')
            ->setHelp('Regénère les thumbs des illustrations')
            ->addArgument('cronid', InputArgument::OPTIONAL, 'ID Cron Job') 
            ->addArgument('lastchance', InputArgument::OPTIONAL, 'Lastchance to run the cron')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";
        $alias              = $this->container->getParameter('appAlias');

        $this->writelnred('');
        $this->writelnred('== app:regenthumb');        
        $this->writelnred('==========================================================================================================');

        $now=new \DateTime('now');

        $directory=$this->container->get('kernel')->getProjectDir()."/public/uploads/illustration";
        $this->writeln($directory);

        $files=[];
        $fs = new Filesystem();

        if($fs->exists($directory)) {
            $finder = new Finder();
            $finder->in($directory)->files();

            foreach (iterator_to_array($finder) as $file) {
                $name   = $file->getRelativePathname();
                if(stripos($name,"thumb_")===false&&stripos($name,"thumbori_")===false) {
                    $width = $this->getWidth($directory.'/'.$name);
                    $height = $this->getHeight($directory.'/'.$name);

                    // Création des miniatures proportionnelle
                    $fs->remove($directory.'/thumbori_'.$name);
                    $fs->copy($directory.'/'.$name,$directory.'/thumbori_'.$name);
                    $max_width=500;
                    $scale = $max_width/$width;
                    $this->resizeImage($directory.'/thumbori_'.$name,$width,$height,$scale);

                    $this->writeln($name);
                }
            }
        }

        $this->writeln('');
        return 1;
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }     


    protected function getHeight($image) {
        $size = getimagesize($image);
        $height = $size[1];
        return $height;
    }

    // Cacul de la largeur
    protected function getWidth($image) {
        $size = getimagesize($image);
        $width = $size[0];
        return $width;
    }

    protected function resizeImage($image,$width,$height,$scale) {
        list($imagewidth, $imageheight, $imageType) = getimagesize($image);
        $imageType = image_type_to_mime_type($imageType);
        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);
        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
        switch($imageType) {
            case "image/gif":
                $source=imagecreatefromgif($image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                $source=imagecreatefromjpeg($image);
                break;
            case "image/png":
            case "image/x-png":
                $source=imagecreatefrompng($image);
                break;
        }

        $newImage = imagecreatetruecolor( $newImageWidth, $newImageHeight );
        imagealphablending( $newImage, false );
        imagesavealpha( $newImage, true );
        imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

        switch($imageType) {
            case "image/gif":
                imagegif($newImage,$image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                imagejpeg($newImage,$image,90);
                break;
            case "image/png":
            case "image/x-png":
                imagepng($newImage,$image);
                break;
        }

        chmod($image, 0640);
        chown($image, 'www-data');
        return $image;
    }    
}
