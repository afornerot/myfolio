<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Finder\Finder;

class UpdateDateCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    private $byexec;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:updatedate')
            ->setDescription('Regnère les dates de création')
            ->setHelp('Regnère les dates de création dans le même ordre mais à la date du jour afin que les illustrations soient vu par le moteur sociale comme nouveau')
            ->addArgument('cronid', InputArgument::OPTIONAL, 'ID Cron Job') 
            ->addArgument('lastchance', InputArgument::OPTIONAL, 'Lastchance to run the cron')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";
        $alias              = $this->container->getParameter('appAlias');

        $this->writelnred('');
        $this->writelnred('== app:updatedate');        
        $this->writelnred('==========================================================================================================');

        $now=new \DateTime('now');
        $illustrations=$this->em->getRepository("App:Illustration")->findAll();
        foreach($illustrations as $illustration) {
            $now->add(new \DateInterval('PT1M'));
            $illustration->setSubmittime($now);
            $this->em->persist($illustration);
            $this->em->flush();            
        }


        $this->writeln('');
        return 1;
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
        if($this->byexec) $this->filesystem->appendToFile($this->rootlog.'exec.log', $string."\n");
    }     
}
