<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Command\LockableTrait;

use App\Entity\Cron;

class CronCommand extends Command
{
    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    use LockableTrait;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:Cron')
            ->setDescription('Execution of the cron command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output       = $output;
        $this->filesystem   = new Filesystem();
        $this->rootlog      = $this->container->get('kernel')->getLogDir()."/";

        $appCron = $this->container->getParameter('appCron');
        if(!$appCron)
        {
            return 0;
        }

        if (!$this->lock()) {
            $this->output->writeln("CRON LOCK");
            return 0;
        }

        $crons = $this->em->getRepository('App:Cron')->toexec();
        $i=0;

        if($crons) {
            $now=new \DateTime();
            $this->writelnred('');
            $this->writelnred('');
            $this->writelnred('');
            $this->writelnred('');
            $this->writelnred('==========================================================================================================');
            $this->writelnred('== CRON ==================================================================================================');
            $this->writelnred('==========================================================================================================');   
            $this->writeln   ('Date = '.$now->format('Y-m-d H:i:s'));   
            $this->writeln   ('Application = '.$this->container->getParameter("appName"));   
        }
      
        foreach($crons as $cron) {
            $i++;
            
            // Id du cron
            $idcron = $cron->getId();

            // Flag d'execution en cours
            $now=new \DateTime();
            $cron->setStartexecdate($now);
            //$cron->setStatut(1);
            $this->em->persist($cron);
            $this->em->flush();

            // Récupération de la commande
            $command = $this->getApplication()->find($cron->getCommand());
            
            // Réccuépration des parametres
            $jsonparameter=json_decode($cron->getJsonargument(),true);

            // Parametre id du cron actuel
            $jsonparameter["cronid"]=$cron->getId();

            // Parametre si dernière execution
            $lastchance=false;
            if($cron->getRepeatcall()>0)
                $lastchance=($cron->getRepeatexec()+1==$cron->getRepeatcall());
            $jsonparameter["lastchance"]=$lastchance;

            // Formater la chaine de parametre
            $parameter = new ArrayInput($jsonparameter);

            // Executer la commande
            $returnCode=false;
            try{
                $returnCode = $command->run($parameter, $output);

                // Revenir sur le cron encours à cause du clear du manager présent dans la synchro
                // Sinon le manager se pomme et génère des nouveaux enregistrement plutot que mettre à jour celui en cours
                $cron=$this->em->getRepository('App:Cron')->find($idcron);
            }
            catch(\Exception $e) { 
                $this->writelnred("JOB EN ERREUR"); 
            }            

            // Flag de fin d'execution
            $now=new \DateTime();
            $cron->setEndexecdate($now);

            // Date prochaine execution
            if($cron->getrepeatinterval()>=0) {
                // Si interval par heure
                if(fmod($cron->getRepeatinterval(),3600)==0) 
                    $next=clone $cron->getNextexecdate();
                else
                    $next=new \DateTime();

                $next->add(new \DateInterval('PT'.$cron->getRepeatinterval().'S'));
                $cron->setNextexecdate($next);
            }
            
            // Statut OK/KO/Retry
            if($returnCode!="retry"||!$returnCode) {
                $cron->setStatut(2);
                if($returnCode!=1) {
                    $cron->setStatut(3);
                    if($cron->getRepeatcall()>0) $cron->setRepeatexec($cron->getRepeatexec()+1);
                }
            }

            $this->em->persist($cron);
            $this->em->flush();
        }

        if($crons) {
            $this->writelnred("==");
            $this->writelnred("FIN CRON");
            $this->writelnred("==");
            $this->writelnred("");
        }

        return 1;
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    } 
}
