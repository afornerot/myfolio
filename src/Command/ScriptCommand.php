<?php

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\ArrayInput;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Id\AssignedGenerator;

use App\Entity\Script;

class ScriptCommand extends Command
{

    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setName('app:Script')
            ->setDescription('Script to call')
            ->setHelp('Script to call')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output           = $output;
        $this->filesystem       = new Filesystem();
        $this->rootlog          = $this->container->get('kernel')->getLogDir()."/";

        $output->writeln('APP = Scripts');

        //$this->callscript("test");

        $output->writeln('');


        return 1;
    }


    private function callscript($name) {    
        $script=$this->em->getRepository("App:Script")->findOneBy(["name"=>$name]);
        if(!$script) {
            $this->writelnred("== SCRIPT = ".$name);
            $this->$name();

            $script=new Script();
            $script->setName($name);
            $this->em->persist($script);
            $this->em->flush();
            $this->writeln("");                  
        }
    }

    private function test() {
        $this->writeln("TEST");
    }



    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }     
}
