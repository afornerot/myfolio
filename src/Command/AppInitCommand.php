<?php

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Id\AssignedGenerator;

use App\Entity\User;
use App\Entity\Category;
use App\Entity\Config;

class AppInitCommand extends Command
{

    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;
    private $appname;
    private $appmailnoreply;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setName('app:AppInit')
            ->setDescription('Init Data for App')
            ->setHelp('This command Init Data App')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output           = $output;
        $this->filesystem       = new Filesystem();
        $this->rootlog          = $this->container->get('kernel')->getLogDir()."/";
        $this->appname          = $this->container->getParameter('appName');
        $this->appmailnoreply   = $this->container->getParameter('appMailnoreply');

        $output->writeln('APP = Default Data');

        // Création du compte admin si non existant
        $this->insertUser("admin","admin");

        // Création d'une catégory par défaut
        $this->insertCategory(-100,1,"Illustrations");

        // colorbgbody = Couleur des fonds de page
        $this->insertConfig(
            1,                                                  // order
            "site",                                             // category
            "appname",                                          // id
            "Titre de votre site",                              // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Titre de votre site"
        );

        $this->insertConfig(
            2,                                                  // order
            "site",                                             // category
            "appsubname",                                       // id
            "Sous-titre de votre site",                         // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Sous-titre de votre site"
        );

        $this->insertConfig(
            3,                                                  // order
            "site",                                             // category
            "appdescription",                                   // id
            "Description de votre site",                        // title
            "",                                                 // value
            "editor",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Description de votre site"
        );

        $this->insertConfig(
            4,                                                  // order
            "site",                                             // category
            "appthumbwidth",                                    // id
            "Largeur des miniatures",                           // title
            "0",                                                // value
            "thumbwidth",                                       // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "" ,                                                // grouped
            "Largeur des miniatures"
        );

        $this->insertConfig(
            5,                                                  // order
            "site",                                             // category
            "appthumbheight",                                   // id
            "Hauteur des miniatures",                           // title
            "0",                                                // value
            "thumbheight",                                      // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "" ,                                                // grouped
            "Largeur des miniatures"
        );

        $this->insertConfig(
            6,                                                  // order
            "site",                                             // category
            "appthumbfilter",                                   // id
            "Filtre sur les miniatures",                        // title
            "0",                                                // value
            "boolean",                                          // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "" ,                                                // grouped
            "Filtre sur les miniatures"
        );

        $this->insertConfig(
            7,                                                  // order
            "site",                                             // category
            "appthumbfiltergrayscale",                          // id
            "Filtre grayscale sur les miniatures",              // title
            "100",                                              // value
            "pourcentage",                                      // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "appthumbfilter" ,                                  // grouped
            "Filtre grayscale sur les miniatures"
        );

        $this->insertConfig(
            8,                                                  // order
            "site",                                             // category
            "appthumbfilteropacity",                            // id
            "Filtre opacity sur les miniatures",                // title
            "100",                                              // value
            "pourcentage",                                      // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "appthumbfilter" ,                                  // grouped
            "Filtre opacity sur les miniatures"
        );

        $this->insertConfig(
            9,                                                  // order
            "site",                                             // category
            "appthumbfiltersepia",                              // id
            "Filtre sepia sur les miniatures",                  // title
            "0",                                                // value
            "pourcentage",                                      // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "appthumbfilter" ,                                  // grouped
            "Filtre sepia sur les miniatures"
        );


        $this->insertConfig(
            10,                                                 // order
            "site",                                             // category
            "appmaxthumbwidth",                                 // id
            "Largeur maximum de la grille des thumbs",          // title
            "0",                                                // value
            "integer",                                          // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "" ,                                                // grouped
            "Largeur maximum de la grille des thumbs\n0 pour indiquer qu'il n'y a pas de taille maximum"
        );

        // colorbgbody = Couleur des fonds de page
        $this->insertConfig(
            1,                                                  // order
            "colorbgbody",                                      // category
            "colorbgbodydark",                                  // id
            "Couleur de fond fonçée",                           // title
            "#343a40",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur de fond quand le site a besoin d'avoir une couleur de fond foncée"
        );

        $this->insertConfig(
            2,                                                  // order
            "colorbgbody",                                      // category
            "colorbgbodylight",                                 // id
            "Couleur de fond claire",                           // title
            "#ffffff",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur de fond quand le site a besoin d'avoir une couleur de fond claire"
        );

        // colorfttitle = Couleur des fontes titre
        $this->insertConfig(
            1,                                                  // order
            "colorfttitle",                                     // category
            "colorfttitledark",                                 // id
            "Couleur des titres sur fond fonçé",                // title
            "#ffffff",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur des titres sur fond fonçé"
        );

        $this->insertConfig(
            2,                                                  // order
            "colorfttitle",                                     // category
            "colorfttitlelight",                                // id
            "Couleur des titres sur fond claire",               // title
            "#343a40",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur des titres sur fond claire"
        );
        
        // colorftbody = Couleur des fontes titre
        $this->insertConfig(
            1,                                                  // order
            "colorftbody",                                      // category
            "colorftbodydark",                                  // id
            "Couleur de la police sur fond fonçé",                  // title
            "#ffffff",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur de la police sur fond fonçé"
        );

        $this->insertConfig(
            2,                                                  // order
            "colorftbody",                                      // category
            "colorftbodylight",                                 // id
            "Couleur de la police sur fond claire",             // title
            "#343a40",                                          // value
            "color",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur de la police sur fond claire"
        );

        // font = nom des polices
        $this->insertConfig(
            1,                                                  // order
            "font",                                             // category
            "fonttitle",                                        // id
            "Police pour les titres",                           // title
            "FredokaOne-Regular",                               // value
            "font",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "La couleur de la police de votre site"
        );        

        $this->insertConfig(
            2,                                                  // order
            "font",                                             // category
            "fontbody",                                         // id
            "Police principale",                                // title
            "Roboto-Regular",                                   // value
            "font",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            true,                                               // required
            "",                                                 // grouped
            "Nom de la police principale"
        );

        // logo = 
        $this->insertConfig(
            1,                                                  // order
            "logo",                                             // category
            "logodark",                                         // id
            "Logo sur fond fonçé",                              // title
            "",                                                 // value
            "logo",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Logo sur fond fonçé"
        );  

        $this->insertConfig(
            2,                                                  // order
            "logo",                                             // category
            "logolight",                                        // id
            "Logo sur fond clair",                              // title
            "",                                                 // value
            "logo",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Logo sur fond clair"
        );  

        // image = 
        $this->insertConfig(
            1,                                                  // order
            "image",                                            // category
            "imgcontact",                                       // id
            "Image Contact",                                    // title
            "",                                                 // value
            "image",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image associée à la section contact"
        );  

        $this->insertConfig(
            1,                                                  // order
            "image",                                            // category
            "imglink",                                          // id
            "Image Liens",                                      // title
            "",                                                 // value
            "image",                                            // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image associée à la section liens"
        );  

        // hero = 
        $this->insertConfig(
            1,                                                  // order
            "hero",                                             // category
            "hero01",                                           // id
            "Carrousel 01",                                     // title
            "",                                                 // value
            "hero",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image 01 de votre carrousel"
        );  

        $this->insertConfig(
            2,                                                  // order
            "hero",                                             // category
            "hero02",                                           // id
            "Carrousel 02",                                     // title
            "",                                                 // value
            "hero",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image 02 de votre carrousel"
        );     
        
        $this->insertConfig(
            3,                                                  // order
            "hero",                                             // category
            "hero03",                                           // id
            "Carrousel 03",                                     // title
            "",                                                 // value
            "hero",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image 03 de votre carrousel"
        );     
        
        $this->insertConfig(
            4,                                                  // order
            "hero",                                             // category
            "hero04",                                           // id
            "Carrousel 04",                                     // title
            "",                                                 // value
            "hero",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image 04 de votre carrousel"
        );          

        $this->insertConfig(
            5,                                                  // order
            "hero",                                             // category
            "hero05",                                           // id
            "Carrousel 05",                                     // title
            "",                                                 // value
            "hero",                                             // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Image 05 de votre carrousel"
        );          

        // Social = 
        $this->insertConfig(
            1,                                                  // order
            "social",                                           // category
            "email",                                            // id
            "Email",                                            // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "L'email du site"
        );  

        $this->insertConfig(
            2,                                                  // order
            "social",                                           // category
            "facebook",                                         // id
            "Facebook",                                         // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Le Facebook du site"
        );  

        $this->insertConfig(
            3,                                                  // order
            "social",                                           // category
            "instagram",                                        // id
            "Instagram",                                        // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Le Instagram du site"
        );  

        $this->insertConfig(
            4,                                                  // order
            "social",                                           // category
            "twitter",                                          // id
            "Twitter",                                          // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Le Twitter du site"
        );  

        $this->insertConfig(
            5,                                                  // order
            "social",                                           // category
            "google",                                           // id
            "Google",                                           // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Le Google du site"
        ); 

        $this->insertConfig(
            6,                                                  // order
            "social",                                           // category
            "youtube",                                          // id
            "Youtube",                                          // title
            "",                                                 // value
            "string",                                           // type,
            true,                                               // visible
            true,                                               // changeable
            false,                                              // required
            "",                                                 // grouped
            "Le Youtube du site"
        ); 

        $output->writeln('');

        return 1;
    }

    protected function insertUser() {
        $metadata =  $this->em->getClassMetaData('App:User');
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new AssignedGenerator());
              
        // Création du compte admin par défaut
        $entity = $this->em->getRepository('App:User')->findOneBy(["username"=>"admin"]);
        if(!$entity) {
            $this->writelnred('Création du compte admin par défaut avec password admin - Veuillez modifier votre password admin après connexion');
            $entity = new User;
            $entity->setId(-100);
            $entity->setUsername("admin");
            $entity->setPassword("admin");
            $entity->setFirstname($this->appname);
            $entity->setLastname("Admin");
            $entity->setEmail($this->appmailnoreply);
            $entity->setRoles(["ROLE_ADMIN"]);
            $entity->setAvatar("admin.jpg");
            $this->em->persist($entity);
        }          

        // On flush
        $this->em->flush();
    }

    protected function insertCategory($id,$order,$name) {
        $metadata =  $this->em->getClassMetaData('App:Category');
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new AssignedGenerator());
              
        // Création du compte admin par défaut
        $entity = $this->em->getRepository('App:Category')->find($id);
        if(!$entity) {
            $entity = new Category;
            $entity->setId($id);
            $entity->setOrder($order);
            $entity->setName($name);
            $entity->setUsecategoryconfig(false);
            $entity->setAppthumbfilter(false);
            $entity->setAppthumbheight(0);
            $entity->setAppthumbwidth(0);
            $entity->setAppthumbfilter(false);
            $entity->setAppthumbfiltergrayscale(100);
            $entity->setAppthumbfilteropacity(100);
            $entity->setAppthumbfiltersepia(0);            
            $this->em->persist($entity);
        }          

        // On flush
        $this->em->flush();
    }    

    protected function insertConfig($order,$category,$id,$title,$value,$type,$visible,$changeable,$required,$grouped,$help) {
        $entity=$this->em->getRepository("App:Config")->find($id);
        if(!$entity) {
            $entity= new Config();
            $entity->setId($id);
            $entity->setValue($value);
        }

        $entity->setCategory($category);
        $entity->setOrder($order);
        $entity->setTitle($title);
        $entity->setType($type);
        $entity->setVisible($visible);
        $entity->setChangeable($changeable);
        $entity->setRequired($required);
        $entity->setGrouped($grouped);
        $entity->setHelp($help);

        $this->em->persist($entity);
        $this->em->flush();
    }

    private function writelnred($string) { 
        $this->output->writeln('<fg=red>'.$string.'</>');
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }
    private function writeln($string) { 
        $this->output->writeln($string);
        $this->filesystem->appendToFile($this->rootlog.'cron.log', $string."\n");
    }     
}
