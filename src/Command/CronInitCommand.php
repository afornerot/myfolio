<?php

namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Id\AssignedGenerator;

use App\Entity\Cron;


class CronInitCommand extends Command
{

    private $container;
    private $em;
    private $output;
    private $filesystem;
    private $rootlog;

    public function __construct(ContainerInterface $container,EntityManagerInterface $em)
    {
        parent::__construct();
        $this->container = $container;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setName('app:CronInit')
            ->setDescription('Init Data for Cron')
            ->setHelp('This command Init Data Cron')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appCron = $this->container->getParameter('appCron');
        $this->rootlog = $this->container->get('kernel')->getLogDir()."/";

        if(!$appCron)
            return false;

        $output->writeln('CRON = Default Data');

        $this->insertCron();

        $output->writeln('');

        return 1;
    }

    protected function insertCron() {
        
        $metadata =  $this->em->getClassMetaData('App:Cron');

        // Job Mail
        // Toute les minutes
        $entity = $this->em->getRepository('App:Cron')->findOneBy(["command"=>"app:sendMail"]);
        if(!$entity) {
            $entity = new Cron;
            $entity->setCommand("app:sendMail");
            $entity->setDescription("Execution du spool de mail en attente");
            $entity->setStatut(2);
            $entity->setRepeatcall(0);
            $entity->setRepeatexec(0);
            $entity->setRepeatinterval(60);
            $entity->setNextexecdate($entity->getSubmitdate());
            $entity->setJsonargument('{"message-limit":"100","env":"prod"}');
            $this->em->persist($entity);
        }

        // Job de purge des fichiers obsolète
        // Toute les 24h à 3h00
        $entity = $this->em->getRepository('App:Cron')->findOneBy(["command"=>"app:purgeFile"]);
        if(!$entity) {
            $entity = new Cron;
            $nextdate=$entity->getSubmitdate();
            $nextdate->setTime(3,0);            
            $entity->setCommand("app:purgeFile");
            $entity->setDescription("Suppression des fichiers obsolètes");
            $entity->setStatut(2);
            $entity->setRepeatcall(0);
            $entity->setRepeatexec(0);
            $entity->setRepeatinterval(86400);
            $entity->setNextexecdate($nextdate);
            $this->em->persist($entity);
        }
        
        // Job Dump
        // Toute les 24h à 2h00
        $entity = $this->em->getRepository('App:Cron')->findOneBy(["command"=>"app:dumpBdd"]);
        if(!$entity) {
            $entity = new Cron;
            $nextdate=$entity->getSubmitdate();
            $nextdate->setTime(2,0);            
            $entity->setCommand("app:dumpBdd");
            $entity->setDescription("Sauvegarde de la BDD");
            $entity->setStatut(2);
            $entity->setRepeatcall(0);
            $entity->setRepeatexec(0);
            $entity->setRepeatinterval(86400);
            $entity->setNextexecdate($nextdate);
            $this->em->persist($entity);
        }        
        
        // On flush
        $this->em->flush();
        
    }
}
