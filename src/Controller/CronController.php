<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use App\Entity\Cron as Entity;
use App\Form\CronType as Form;

class CronController extends AbstractController
{
    private $data   = "cron";
    private $route  = "app_cron";
    private $render = "Cron/";
    private $entity = "App:Cron";

    public function list()
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);   

    }

    public function update($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,    
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView()
        ]);
    } 

    public function log()
    {
        return $this->render($this->render.'logs.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,   
        ]);
    }    

    public function getlog(Request $request, $id)
    {
        
        $path = $this->getParameter('kernel.project_dir');
        if($id=="dump")
            $file = $path . '/var/log/' . $this->getParameter("appAlias") . '.sql';
        else
            $file = $path . '/var/log/'.$id.'.log';
        
        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;
    }    
    
    
    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }     
}
