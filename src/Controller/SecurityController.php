<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Group;
use App\Service\ldapService as ldapService; 
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends AbstractController
{
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $auth_mode=$this->getParameter("appAuth");
        switch($auth_mode) {
            case "MYSQL":
                return $this->loginMYSQL($request,$authenticationUtils);
            break;

            case "CAS":
                return $this->loginCAS($request,$authenticationUtils);
            break;
        }
    }           

    public function loginMYSQL(Request $request, AuthenticationUtils $authenticationUtils) {
        return $this->render('Home/login.html.twig', array(
            'last_username' =>  $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'usemonocolor' => true,
        ));

    }

    public function loginCAS(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // Récupération de la cible de navigation        
        $redirect = $this->get('session')->get("_security.main.target_path");

        // Init Client CAS
        $alias=$this->getParameter('appAlias');
        \phpCAS::setDebug('/var/www/html/'.$alias.'/var/log/cas.log');
        \phpCAS::client(CAS_VERSION_2_0, $this->getParameter('casHost'), intval($this->getParameter('casPort')), is_null($this->getParameter('casPath')) ? '' : $this->getParameter('casPath'), false);
        \phpCAS::setNoCasServerValidation();
        

        // Authentification
        \phpCAS::forceAuthentication();

        // Récupération UID
        $username = \phpCAS::getUser();

        // Récupération Attribut
        $attributes = \phpCAS::getAttributes();      

        // Rechercher l'utilisateur
        $em = $this->getDoctrine()->getManager();
        if(isset($attributes[$this->getParameter('casUsername')]))
            $username = $attributes[$this->getParameter('casUsername')];
        
        if(isset($attributes[$this->getParameter('casEmail')]))
            $email = $attributes[$this->getParameter('casEmail')];
        
        if(isset($attributes[$this->getParameter('casLastname')]))
            $lastname = $attributes[$this->getParameter('casLastname')];
        
        if(isset($attributes[$this->getParameter('casFirstname')]))
            $firstname = $attributes[$this->getParameter('casFirstname')];

        $user = $em->getRepository('App:User')->findOneBy(array("username"=>$username));
        $exists = $user ? true : false;

        if (!$exists) {
            $user = new User();

            $user->setUsername($username);
            $user->setLastname($lastname);
            $user->setFirstname($firstname);
            $user->setEmail($email);

            $user->setPassword("CASPWD-".$username);
            $user->setSalt("CASPWD-".$username);
                        
            $em->persist($user);
            $em->flush();
        }
        else {
            if(isset($lastname)) $user->setLastname($lastname);
            if(isset($firstname)) $user->setFirstname($firstname);
            if(isset($email)) $user->setEmail($email);
            
            $em->persist($user);
            $em->flush();
        }

        // Sauvegarde des attributes en session
        $this->get('session')->set('attributes', $attributes);

        // Autoconnexion
        // Récupérer le token de l'utilisateur
        $token = new UsernamePasswordToken($user, null, "main", $user->getRoles());
        $this->get("security.token_storage")->setToken($token);

        // Simuler l'evenement de connexion
        $event = new InteractiveLoginEvent($request, $token);
        $dispatcher = new EventDispatcher();
        $dispatcher->dispatch($event);

        // Redirection
        if($redirect)
            return $this->redirect($redirect);
        else
            return $this->redirect($this->generateUrl('app_home'));
    }



    public function logout() {
        $auth_mode=$this->getParameter("appAuth");
        switch($auth_mode) {
            case "MYSQL":
                return $this->logoutMYSQL();
            break;

            case "CAS":
                return $this->logoutCAS();
            break;
        }

    }

    public function logoutMYSQL() {
        $this->get('security.token_storage')->setToken(null);
        $this->get('session')->invalidate();

        return $this->redirect($this->generateUrl("app_home"));   
    }

    public function logoutcas() {
        $this->get('security.token_storage')->setToken(null);
        $this->get('session')->invalidate();

        // Init Client CAS
        $alias=$this->getParameter('appAlias');
        \phpCAS::setDebug('/var/www/html/'.$alias.'/var/log/cas.log');
        \phpCAS::client(CAS_VERSION_2_0, $this->getParameter('casHost'), intval($this->getParameter('casPort')), is_null($this->getParameter('casPath')) ? '' : $this->getParameter('casPath'), false);
        \phpCAS::setNoCasServerValidation();


        // Logout
        $url=$this->generateUrl('app_home', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        \phpCAS::logout(array("service"=>$url));
        
        return true;
    }
}
