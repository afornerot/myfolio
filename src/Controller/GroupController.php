<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Group as Entity;
use App\Form\GroupType as Form;

class GroupController extends AbstractController
{
    private $data   = "group";
    private $route  = "app_group";
    private $render = "Group/";
    private $entity = "App:Group";

    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function submit(Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data = new Entity();
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"submit"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"submit");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,       
            $this->data         => $data,
            'mode'              => 'submit',
            'form'              => $form->createView()
        ]);
    }  
    
    public function update($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update"));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm($id,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,  
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView()
        ]);
    }  

    public function delete($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Controle avant suppression
        $error=false;
        if($id<0) $error=true;
        
        if($error)
            return $this->redirectToRoute($this->route."_update",["id"=>$id]);
        else {
            $em->remove($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
    } 

    public function select(Request $request)
    {
        // S'assurer que c'est un appel ajax
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Interdit'), 400);
        }

        $output=array();
        $em = $this->getDoctrine()->getManager();
        $page_limit=$request->query->get('page_limit');
        $q=$request->query->get('q');
        
        $qb = $em->createQueryBuilder();
        if($this->getUser()->Hasrole("ROLE_ADMIN")) {
            $qb->select('table')->from($this->entity,'table')
            ->where('table.name LIKE :value')
            ->setParameter("value", "%".$q."%")
            ->orderBy('table.name');
        }
        else {
            $qb->select('table')->from($this->entity,'table')
            ->where('table.name LIKE :value')
            ->andwhere(":user MEMBER OF table.users")
            ->setParameter("value", "%".$q."%")
            ->setParameter("user",$this->getUser())
            ->orderBy('table.name');
        }

        $datas=$qb->setFirstResult(0)->setMaxResults($page_limit)->getQuery()->getResult();
        foreach($datas as $data) {
            array_push($output,array("id"=>$data->getId(),"text"=>$data->getName()));
        }

        $ret_string["results"]=$output;
        $response = new Response(json_encode($ret_string));    
        $response->headers->set('Content-Type', 'application/json');      
        return $response;
    }

    public function info(Request $request)
    {
        // S'assurer que c'est un appel ajax
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Interdit'), 400);
        }

        $output=array();
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $group=$em->getRepository($this->entity)->find($id);
        $users=$group->getUsers();

        $output=[];
        $output["id"]           = $group->getId();
        $output["name"]         = $group->getName();
        $output["users"]        = [];

        foreach($users as $user) {
            $tmp=[];
            $tmp["id"]          = $user->getId();
            $tmp["displayname"] = $user->getId();
            $tmp["email"]       = $user->getEmail();

            array_push($output["users"],$tmp);
        }

        $response = new Response(json_encode($output));    
        $response->headers->set('Content-Type', 'application/json');      
        return $response;
    }

    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && ($mode=="submit" || $mode=="update")) {
            // On s'assure que le label ne contient pas des caractères speciaux
            $string = preg_replace('~[^ éèêôöàïî\'@a-zA-Z0-9._-]~', '', $data->getName());
            if($string!=$data->getName())
            {
                $form->addError(new FormError('Caractères interdit dans ce label'));
            } 
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }     
}
