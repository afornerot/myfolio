<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Filesystem\Filesystem;

class CropController extends AbstractController
{
    // Etape 01 - Téléchargement de l'image
    public function crop01($type,$reportinput)
    {
        return $this->render('Crop/crop01.html.twig',[
            'useheader'     => false,
            'usesidebar'    => false,             
            'type'          => $type,
            'reportinput'   => $reportinput
        ]);
    }

    // Etape 02 - Couper votre l'image
    public function crop02($type,$reportinput,Request $request)
    {
        // Récupération de l'image à cropper
		$file=$request->query->get('file');
	    $large_image_location = "uploads/$type/$file";

        // Récupérer les tailles de l'image
		$width = $this->getWidth($large_image_location);
		$height = $this->getHeight($large_image_location);

		// Définir le pourcentage de réduction de l'image
		switch ($type) {
			case "illustration":
				$max_height=0;
				$ratio="1:1";
				break;

			case "avatar":
				$max_height=900;
				$max_width=900;
				$ratio="1:1";
				break;

			case "hero":
				$max_height=1600;
				$max_width=1600;
				$ratio="16:9";
				break;
			
			case "image":
				$max_height=1600;
				$max_width=1600;
				$ratio="1:1";
				break;				
        }
                
		if($max_height>0) {
			$scale = $max_height/$height;
			if(($width*$scale)>$max_width) {
				$scale = $max_width/$width;
			}
			$this->resizeImage($large_image_location,$width,$height,$scale);
		}
		else $scale=1;

        // Construction du formulaire
        $submited=false;
        $form = $this->createFormBuilder()
            ->add('submit',SubmitType::class,array("label" => "Valider","attr" => array("class" => "btn btn-success","onclick" => "reportThumb()")))
            ->add('x',HiddenType::class)
            ->add('y',HiddenType::class)
            ->add('w',HiddenType::class)
            ->add('h',HiddenType::class)
            ->add('xs',HiddenType::class)
            ->add('ys',HiddenType::class)
            ->add('ws',HiddenType::class)
            ->add('hs',HiddenType::class)            
            ->getForm();

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur validation on généère la miniature croppée
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            // Récupération des valeurs du formulaire
            $data = $form->getData();
		    $thumb_image_location = "uploads/$type/thumb_".$file;
            $cropped = $this->resizeThumbnailImage($thumb_image_location, $large_image_location,$data["ws"],$data["hs"],$data["xs"],$data["ys"],$scale);
            $submited=true;
        }

        return $this->render('Crop/crop02.html.twig', [
            'useheader'     => false,
            'usesidebar'    => false,
			'form' 			=> $form->createView(),
			'type' 			=> $type,
			'file' 			=> $file,
			'ratio'			=> $ratio,
			"reportinput"	=> $reportinput,
			"submited"		=> $submited         
        ]);
    }

	// Calcul de la hauteur
	protected function getHeight($image) {
		$size = getimagesize($image);
		$height = $size[1];
		return $height;
	}

	// Cacul de la largeur
	protected function getWidth($image) {
		$size = getimagesize($image);
		$width = $size[0];
		return $width;
	}

	protected function resizeImage($image,$width,$height,$scale) {
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);
		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image);
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image);
				break;
		}
		imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$image,90);
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$image);
				break;
		}

		chmod($image, 0640);
		return $image;
	}

	protected function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
        $fs = new Filesystem();
        $fs->remove($thumb_image_name);
        
		list($imagewidth, $imageheight, $imageType) = getimagesize($image);
		$imageType = image_type_to_mime_type($imageType);

		$newImageWidth = ceil($width * $scale);
		$newImageHeight = ceil($height * $scale);
		$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
		switch($imageType) {
			case "image/gif":
				$source=imagecreatefromgif($image);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				$source=imagecreatefromjpeg($image);
				break;
			case "image/png":
			case "image/x-png":
				$source=imagecreatefrompng($image);
				break;
		}
		imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
		switch($imageType) {
			case "image/gif":
				imagegif($newImage,$thumb_image_name);
				break;
			case "image/pjpeg":
			case "image/jpeg":
			case "image/jpg":
				imagejpeg($newImage,$thumb_image_name,90);
				break;
			case "image/png":
			case "image/x-png":
				imagepng($newImage,$thumb_image_name);
				break;
		}
		chmod($thumb_image_name, 0640);
		return $thumb_image_name;
	}

}
