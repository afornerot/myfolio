<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Config as Entity;
use App\Form\ConfigType as Form;

class ConfigController extends AbstractController
{
    private $data   = "config";
    private $route  = "app_config";
    private $render = "Config/";
    private $entity = "App:Config";

    public function list()
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findBy(["visible"=>true]);

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function listrender($category)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findBy(["visible"=>true,"category"=>$category]);

        return $this->render($this->render.'render.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function update($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update","id"=>$data->getId(),"type"=>$data->getType(),"required"=>$data->getRequired()));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm($id,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,       
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView()
        ]);
    }

    public function delete($id,Request $request)
    {
        // Récupération de l'enregistrement courant
        $em = $this->getDoctrine()->getManager();
        $config=$em->getRepository($this->entity)->find($id);
        if(!$config->getRequired()) {
            $config->setValue("");
            $em = $this->getDoctrine()->getManager();
            $em->persist($config);
            $em->flush();            
        }
        return $this->redirectToRoute('app_config');
    }

    public function logo()
    {
        return $this->render($this->render.'logo.html.twig');
    }

    protected function getErrorForm($id,$form,$request,$data) {
        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();
            $validator = $this->get('validator');
            $errors = $validator->validate($data);
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }
}
