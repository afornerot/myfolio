<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Illustration as Entity;
use App\Form\IllustrationType as Form;

class IllustrationController extends AbstractController
{
    private $data   = "illustration";
    private $route  = "app_illustration";
    private $render = "Illustration/";
    private $entity = "App:Illustration";
    
    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }
        
    public function view($idcat,$id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        $datanext=$this->getDataAllNext($idcat,$id);
        $dataprev=$this->getDataAllPrev($idcat,$id);

        $pathinfo=pathinfo($data->getIllustration());

        if(!$data) return $this->redirectToRoute('app_home');

        return $this->render($this->render.'view.html.twig', array(
            $this->data         => $data,
            "prev"              => $dataprev,
            "next"              => $datanext,
            "pathinfo"          => $pathinfo,
            "usemonocolor"      => true,
        ));
    }


    public function submit($by, Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data = new Entity();
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"submit"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"submit");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $data->setSubmittime(new \DateTime());
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute("app_home");
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => false,       
            $this->data         => $data,
            'mode'              => 'submit',
            'form'              => $form->createView(),
            'by'                => $by,
        ]);
    }  
    
    public function update($id,$by,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update"));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm($id,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à l'illustration
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute($this->route.'_view',array("idcat"=>$data->getCategory()->getId(),"id"=> $id));
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => false,  
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView(),
            'by'                => $by,
        ]);
    }  

    public function delete($id,$by,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Controle avant suppression
        $error=false;
        if($id<0) $error=true;
        
        if($error)
            return $this->redirectToRoute($this->route."_update",["id"=>$id]);
        else {
            $em->remove($data);
            $em->flush();

            // Retour à la liste
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute("app_home");
        }
    } 

    public function upload()
    {
        return $this->render($this->render.'upload.html.twig');
    }




    protected function getDataAllNext($idcat,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->createQueryBuilder()
                      ->select('e')
                      ->from($this->entity,  'e')
                      ->where('e.id>:id')
                      ->andWhere('e.category=:idcat')
                      ->getQuery()
                      ->setParameter("id", $id)
                      ->setParameter("idcat", $idcat)
                      ->setMaxResults(1)
                      ->getResult();

        // Si pas de suivant on recherche le premier
        if(!$data) {
            $data = $em->createQueryBuilder()
                        ->select('e')
                        ->from($this->entity,  'e')
                        ->Where('e.category=:idcat')
                        ->getQuery()
                        ->setParameter("idcat", $idcat)
                        ->setMaxResults(1)
                        ->getResult();
        }
        
        return $data;
    }

    protected function getDataAllPrev($idcat,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->createQueryBuilder()
                      ->select('e')
                      ->from($this->entity,  'e')
                      ->where('e.id<:id')
                      ->andWhere('e.category=:idcat')
                      ->orderBy('e.id','DESC')
                      ->getQuery()
                      ->setParameter("id", $id)
                      ->setParameter("idcat", $idcat)
                      ->setMaxResults(1)
                      ->getResult();

        // Si pas de précedent on recherche le dernier
        if(!$data) {
            $data = $em->createQueryBuilder()
                        ->select('e')
                        ->from($this->entity,  'e')
                        ->Where('e.category=:idcat')
                        ->orderBy('e.id','DESC')
                        ->getQuery()
                        ->setParameter("idcat", $idcat)
                        ->setMaxResults(1)
                        ->getResult();
        }

        return $data;
    }

    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && ($mode=="submit" || $mode=="update")) {
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }     
}
