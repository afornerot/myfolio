<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Link as Entity;
use App\Form\LinkType as Form;

class LinkController extends AbstractController
{
    private $data   = "link";
    private $route  = "app_link";
    private $render = "Link/";
    private $entity = "App:Link";

    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function submit(Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data = new Entity();
        $data->setOrder(1);
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"submit"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"submit");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,       
            $this->data         => $data,
            'mode'              => 'submit',
            'form'              => $form->createView()
        ]);
    }  
    
    public function update($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update"));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm($id,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,  
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView()
        ]);
    }  

    public function delete($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Controle avant suppression
        $error=false;
        if($id<0) $error=true;
        
        if($error)
            return $this->redirectToRoute($this->route."_update",["id"=>$id]);
        else {
            $em->remove($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
    } 

    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && ($mode=="submit" || $mode=="update")) {
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }     
}
