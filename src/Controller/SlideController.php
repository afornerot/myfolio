<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Illustration as Entity;
use App\Form\IllustrationType as Form;

class SlideController extends AbstractController
{
    private $data   = "illustration";
    private $route  = "app_illustration";
    private $render = "Illustration/";
    private $entity = "App:Illustration";
    
    public function slide(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'slide.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => false,
            "usesidebar"    => false,
            "usemonocolor"  => true,
        ]);
    }
}
