<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\User as Entity;
use App\Form\UserType as Form;

class UserController extends AbstractController
{
    private $data   = "user";
    private $route  = "app_user";
    private $render = "User/";
    private $entity = "App:User";

    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function submit(Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data = new Entity();
        $data->setAvatar("noavatar.png");
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"submit"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"submit");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,       
            $this->data         => $data,
            'mode'              => 'submit',
            'form'              => $form->createView()
        ]);
    } 

    public function update($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);
        $oldpassword=$data->getPassword();

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update","appAuth"=>$this->getParameter("appAuth")));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  

            // Si pas de changement de password on replace l'ancien
            if(!$data->getPassword()) {
                $data->setPassword($oldpassword);    
            }

            $em->persist($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => true,     
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView()
        ]);
    }  

    public function delete($id,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Controle avant suppression
        $error=false;
        if($id<0) $error=true;
        
        if($error)
            return $this->redirectToRoute($this->route."_update",["id"=>$id]);
        else {
            $em->remove($data);
            $em->flush();

            // Retour à la liste
            return $this->redirectToRoute($this->route);
        }
    } 
        
    public function profil(Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$this->getUser();
        $oldpassword=$data->getPassword();

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"profil","appAuth"=>$this->getParameter("appAuth")));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  

            // Si pas de changement de password on replace l'ancien
            if(!$data->getPassword()) {
                $data->setPassword($oldpassword);    
            }

            $em->persist($data);
            $em->flush();   

            // Retour à la liste
            return $this->redirectToRoute("app_home");
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => false,     
            $this->data         => $data,
            'mode'              => 'profil',
            'form'              => $form->createView()
        ]);
    }  

    public function select(Request $request)
    {
        // S'assurer que c'est un appel ajax
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Interdit'), 400);
        }

        $output=array();
        $em = $this->getDoctrine()->getManager();
        $page_limit=$request->query->get('page_limit');
        $q=$request->query->get('q');
        
        $qb = $em->createQueryBuilder();
        $qb->select('table')->from($this->entity,'table')
           ->where('table.lastname LIKE :value')
           ->where('table.firstname LIKE :value')
           ->setParameter("value", "%".$q."%")
           ->orderBy('table.firstname')
           ->orderBy('table.lastname');
        
        $datas=$qb->setFirstResult(0)->setMaxResults($page_limit)->getQuery()->getResult();
        foreach($datas as $data) {
            array_push($output,array("id"=>$data->getId(),"text"=>$data->getDisplayname()));
        }

        $ret_string["results"]=$output;
        $response = new Response(json_encode($ret_string));    
        $response->headers->set('Content-Type', 'application/json');      
        return $response;
    }
    
    public function info(Request $request)
    {
        // S'assurer que c'est un appel ajax
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Interdit'), 400);
        }

        $output=array();
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $user=$em->getRepository($this->entity)->find($id);
        $output=[];
        $output["id"]           = $user->getId();
        $output["displayname"]  = $user->getDisplayname();
        $output["email"]        = $user->getEmail();

        $response = new Response(json_encode($output));    
        $response->headers->set('Content-Type', 'application/json');      
        return $response;
    }

    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }
}
