<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use App\Entity\Webzine as Entity;
use App\Form\WebzineType as Form;
use App\Entity\Webzinepage as Webzinepage;

class WebzineController extends AbstractController
{
    private $data   = "webzine";
    private $route  = "app_webzine";
    private $render = "Webzine/";
    private $entity = "App:Webzine";
    
    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($this->entity)->findAll();

        return $this->render($this->render.'list.html.twig',[
            $this->data."s" => $datas,
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }
        
    public function view($idcat,$id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $webzine=$em->getRepository($this->entity)->find($idcat);
        $page=$em->getRepository("App:Webzinepage")->find($id);
        $sets=$em->getRepository($this->entity)->findBy(["set"=>$webzine->getSet()],["order"=>"ASC"]);

        $datanext=$this->getDataAllNext($idcat,$id,$webzine);
        $dataprev=$this->getDataAllPrev($idcat,$id,$webzine);

        $pathinfo=pathinfo($page->getIllustration());

        if(!$page) return $this->redirectToRoute('app_home');

        return $this->render($this->render.'view.html.twig', array(
            $this->data         => $webzine,
            "page"              => $page,
            "sets"              => $sets,
            "prev"              => $dataprev,
            "next"              => $datanext,
            "pathinfo"          => $pathinfo,
            "usemonocolor"      => true,
        ));
    }


    public function submit($by, Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data = new Entity();
        
        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"submit"));

        // Récupération des data du formulaire
        $form->handleRequest($request);
        
        // Sur erreur
        $this->getErrorForm(null,$form,$request,$data,"submit");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  
            $data->setSubmittime(new \DateTime());
            $em->persist($data);
            $em->flush();

            // On récupère les pages et on cacule ceux à ajouter ou à supprimer
            $lstpages=array_filter(explode(",",$form->get("linkpages")->getData()));

            // Récupération de l'id du webzine
            $id=$data->getId();
            $order=0;
            foreach($lstpages as $pageurl) {
                $order++;
                $width=$this->getWidth("uploads/webzine/".$pageurl);
                $height=$this->getHeight("uploads/webzine/".$pageurl);

                $page= new Webzinepage();
                $page->setOrder($order);
                $page->setWidth($width);
                $page->setHeight($height);
                $page->setIllustration($pageurl);
                $page->setWebzine($data);

                $em->persist($page);
                $em->flush();
            }

            // Retour à la liste
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute("app_home");
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => false,       
            $this->data         => $data,
            'mode'              => 'submit',
            'form'              => $form->createView(),
            'by'                => $by,
        ]);
    }  
    
    public function update($id,$by,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);
        $oldlstpages = array();
        foreach($data->getWebzinepages() as $page){
            $oldlstpages[] = $page->getIllustration();
        }

        // Création du formulaire
        $form = $this->createForm(Form::class,$data,array("mode"=>"update"));

        // Récupération des data du formulaire
        $form->handleRequest($request);

        // Sur erreur
        $this->getErrorForm($id,$form,$request,$data,"update");
        
        // Sur validation
        if ($form->get('submit')->isClicked() && $form->isValid()) {  
            $data = $form->getData();  

            // On récupère les pages et on cacule ceux à ajouter ou à supprimer
            $lstpages=array_filter(explode(",",$form->get("linkpages")->getData()));
            $removepages=array_diff($oldlstpages,$lstpages);
            $addpages=array_diff($lstpages,$oldlstpages);

            // Suppression des pages obsolètes
            $fggotofirst=false;
            foreach($removepages as $pageurl) {
                $pagetoremove=$em->getRepository("App:Webzinepage")->findOneBy(["illustration"=>$pageurl]);
                $data->removeWebzinepage($pagetoremove);
            }

            // Ajout des nouveaux pages
            $order=0;
            foreach($addpages as $pageurl) {
                $width=$this->getWidth("uploads/webzine/".$pageurl);
                $height=$this->getHeight("uploads/webzine/".$pageurl);

                $page= new Webzinepage();
                $page->setWidth($width);
                $page->setHeight($height);
                $page->setIllustration($pageurl);
                $page->setWebzine($data);

                $em->persist($page);
                $em->flush();
            }

            // Ordonner les pages
            $order=0;
            foreach($lstpages as $pageurl) {
                $order++;
                $page=$em->getRepository("App:Webzinepage")->findOneBy(["illustration"=>$pageurl]);
                $page->setOrder($order);
                $em->persist($page);
                $em->flush();
            }


            $em->persist($data);
            $em->flush();


            // Retour à l'webzine
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute($this->route.'_view',array("idcat"=>$data->getId(),"id"=> $data->getWebzinepages()[0]->getId()));
        }
        
        // Affichage du formulaire
        return $this->render($this->render.'edit.html.twig', [
            'useheader'         => true,
            'usesidebar'        => false,  
            $this->data         => $data,
            'mode'              => 'update',
            'form'              => $form->createView(),
            'by'                => $by,
        ]);
    }  

    public function delete($id,$by,Request $request)
    {
        // Initialisation de l'enregistrement
        $em = $this->getDoctrine()->getManager();
        $data=$em->getRepository($this->entity)->find($id);

        // Controle avant suppression
        $error=false;
        if($id<0) $error=true;
        
        if($error)
            return $this->redirectToRoute($this->route."_update",["id"=>$id]);
        else {
            $em->remove($data);
            $em->flush();

            // Retour à la liste
            if($by=="console") 
                return $this->redirectToRoute($this->route);
            else
                return $this->redirectToRoute("app_home");
        }
    } 

    public function upload()
    {
        return $this->render($this->render.'upload.html.twig');
    }




    protected function getDataAllNext($idcat,$id,$webzine)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->createQueryBuilder()
                      ->select('e')
                      ->from("App:Webzinepage",  'e')
                      ->where('e.id>:id')
                      ->andWhere('e.webzine=:idcat')
                      ->orderBy("e.order","ASC")
                      ->getQuery()
                      ->setParameter("id", $id)
                      ->setParameter("idcat", $idcat)
                      ->setMaxResults(1)
                      ->getResult();
        
        // Si on ne trouve pas de suivant on prend le premier du prochain set
        if(!$data) {
            $next = $em->createQueryBuilder()
                    ->select('w')
                    ->from("App:Webzine", 'w')
                    ->where('w.set=:set')
                    ->andWhere('w.order>:order')
                    ->orderBy("w.order","ASC")
                    ->getQuery()
                    ->setParameter("set", $webzine->getSet())
                    ->setParameter("order", $webzine->getOrder())
                    ->setMaxResults(1)
                    ->getResult();
            if($next) {
                $data = $em->createQueryBuilder()
                    ->select('e')
                    ->from("App:Webzinepage",  'e')
                    ->Where('e.webzine=:idcat')
                    ->orderBy("e.order","ASC")
                    ->getQuery()
                    ->setParameter("idcat", $next[0]->getId())
                    ->setMaxResults(1)
                    ->getResult();

            }
        }
        return $data;
    }

    protected function getDataAllPrev($idcat,$id,$webzine)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->createQueryBuilder()
                      ->select('e')
                      ->from("App:Webzinepage",  'e')
                      ->where('e.id<:id')
                      ->andWhere('e.webzine=:idcat')
                      ->orderBy("e.order","DESC")
                      ->getQuery()
                      ->setParameter("id", $id)
                      ->setParameter("idcat", $idcat)
                      ->setMaxResults(1)
                      ->getResult();
        // Si on ne trouve pas de précédent on prend le dernier du précédent set
        if(!$data) {
            $next = $em->createQueryBuilder()
                    ->select('w')
                    ->from("App:Webzine", 'w')
                    ->where('w.set=:set')
                    ->andWhere('w.order<:order')
                    ->orderBy("w.order","DESC")
                    ->getQuery()
                    ->setParameter("set", $webzine->getSet())
                    ->setParameter("order", $webzine->getOrder())
                    ->setMaxResults(1)
                    ->getResult();
            if($next) {
                $data = $em->createQueryBuilder()
                    ->select('e')
                    ->from("App:Webzinepage",  'e')
                    ->Where('e.webzine=:idcat')
                    ->orderBy("e.order","DESC")
                    ->getQuery()
                    ->setParameter("idcat", $next[0]->getId())
                    ->setMaxResults(1)
                    ->getResult();

            }
        }
        return $data;
    }
    
    protected function getHeight($image) {
        $size = getimagesize($image);
        $height = $size[1];
        return $height;
    }

    // Cacul de la largeur
    protected function getWidth($image) {
        $size = getimagesize($image);
        $width = $size[0];
        return $width;
    }

    protected function getErrorForm($id,$form,$request,$data,$mode) {
        if ($form->get('submit')->isClicked()&&$mode=="delete") {
        }

        if ($form->get('submit')->isClicked() && $mode=="submit") {
        }

        if ($form->get('submit')->isClicked() && ($mode=="submit" || $mode=="update")) {
        }

        if ($form->get('submit')->isClicked() && !$form->isValid()) {
            $this->get('session')->getFlashBag()->clear();

            $errors = $form->getErrors();
            foreach( $errors as $error ) {
                $request->getSession()->getFlashBag()->add("error", $error->getMessage());
            }
        }
    }     
}
