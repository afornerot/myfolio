<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;

class HomeController extends AbstractController
{
    public function home()
    {
        $em = $this->getDoctrine()->getManager();
        $categorys = $em->getRepository("App:Category")->findAll();
        $links = $em->getRepository("App:Link")->findAll();
        $webzines = $em->getRepository("App:Webzine")->findBy([], ['set' => 'ASC', 'order' => 'ASC']);
        
        return $this->render('Home/home.html.twig',[
            "useheader"     => false,
            "usesidebar"    => false,
            "usemonocolor"  => true,
            "categorys"     => $categorys,
            "links"         => $links,
            "webzines"      => $webzines
        ]);
    }

    public function feed($nb)
    {
        $feeds=[];

        $em = $this->getDoctrine()->getManager();

        $qb = $em   ->createQueryBuilder()
                    ->select('e')
                    ->from("App:Illustration",  'e');
        if($nb!=0) $qb->setMaxResults($nb);
        $illustrations=$qb->getQuery()->getResult();
        foreach($illustrations as $illustration) {
            $tmp["path"]            = "app_illustration_view";
            $tmp["type"]            = "illustration";
            $tmp["idcat"]           = $illustration->getCategory()->getId();
            $tmp["cat"]             = $illustration->getCategory()->getName();
            $tmp["id"]              = $illustration->getId();
            $tmp["name"]            = $illustration->getName();
            $tmp["illustration"]    = $illustration->getIllustration();
            $tmp["pubtime"]         = $illustration->getSubmittime();
            $tmp["description"]     = $illustration->getDescription();

            array_push($feeds,$tmp);
        }

        

        $qb = $em   ->createQueryBuilder()
                    ->select('e')
                    ->from("App:Webzine",  'e');
        if($nb!=0) $qb->setMaxResults($nb);
        $webzines=$qb->getQuery()->getResult();
        foreach($webzines as $webzine) {
            if($webzine->getWebzinepages()) {
                $tmp["path"]            = "app_webzine_view";
                $tmp["type"]            = "webzine";
                $tmp["idcat"]           = $webzine->getId();
                $tmp["cat"]             = "Webzine".($webzine->getSet()?" - ".$webzine->getSet():"");
                $tmp["id"]              = $webzine->getWebzinepages()[0]->getId();
                $tmp["name"]            = $webzine->getName();
                $tmp["illustration"]    = $webzine->getWebzinepages()[0]->getIllustration();
                $tmp["pubtime"]         = $webzine->getSubmittime();
                $tmp["description"]     = $webzine->getDescription();

                array_push($feeds,$tmp);
            }
        }

        $columns = array_column($feeds, 'pubtime');
        array_multisort($columns, SORT_DESC, $feeds);

        $response = new Response($this->renderView('Home/feed.xml.twig',["feeds" => $feeds]));

        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');
        return $response;
    }    

    public function admin()
    {
        return $this->render('Home/admin.html.twig',[
            "useheader"     => true,
            "usesidebar"    => true,
        ]);
    }

    public function upload(Request $request,$access=null) {
        // Fichier temporaire uploadé
        $tmpfile    = $request->files->get('upload');
        $extention  = $tmpfile->getClientOriginalExtension();

        // Répertoire de Destination
        $fs         = new Filesystem();
        $rootdir    = $this->getParameter('kernel.project_dir') . '/public'; 
        $fs->mkdir($rootdir."/uploads/ckeditor");
        
        // Fichier cible
        $targetName = uniqid().".".$extention;
        $targetFile = $rootdir."/uploads/ckeditor/".$targetName;
        $targetUrl  = "/".$this->getParameter('appAlias')."/uploads/ckeditor/".$targetName;
        $message    = "";

        move_uploaded_file($tmpfile,$targetFile);
        
        $output["uploaded"]=1;
        $output["fileName"]=$targetName;
        $output["url"]=$targetUrl;

        return new Response(json_encode($output));
    }

}
