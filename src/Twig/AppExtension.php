<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    protected $container;

    public function getFilters()
    {
        return [
            new TwigFilter('urlavatar', [$this, 'urlAvatar']),
        ];
    }

    public function urlAvatar($avatar)
    {
        if(stripos($avatar,"http")===0)
            return $avatar;
        else
            return "/".$this->container->getParameter("appAlias")."/uploads/avatar/".$avatar;
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }    
}