<?php
namespace App\Service;

use Doctrine\ORM\EntityManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class uploadListener
{
    private $em;
    private $session;
    private $token;
    private $container;

    public function __construct(EntityManager $em, TokenStorageInterface $token_storage, Session $session, Container $container)
    {
        $this->em = $em;
        $this->session = $session;
        $this->token = $token_storage;
        $this->container = $container;
    }

    protected function getHeight($image) {
        $size = getimagesize($image);
        $height = $size[1];
        return $height;
    }

    // Cacul de la largeur
    protected function getWidth($image) {
        $size = getimagesize($image);
        $width = $size[0];
        return $width;
    }

    protected function resizeImage($image,$width,$height,$scale) {
        list($imagewidth, $imageheight, $imageType) = getimagesize($image);
        $imageType = image_type_to_mime_type($imageType);
        $newImageWidth = ceil($width * $scale);
        $newImageHeight = ceil($height * $scale);
        $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
        switch($imageType) {
            case "image/gif":
                $source=imagecreatefromgif($image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                $source=imagecreatefromjpeg($image);
                break;
            case "image/png":
            case "image/x-png":
                $source=imagecreatefrompng($image);
                break;
        }

        $newImage = imagecreatetruecolor( $newImageWidth, $newImageHeight );
        imagealphablending( $newImage, false );
        imagesavealpha( $newImage, true );
        imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

        switch($imageType) {
            case "image/gif":
                imagegif($newImage,$image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                imagejpeg($newImage,$image,90);
                break;
            case "image/png":
            case "image/x-png":
                imagepng($newImage,$image);
                break;
        }

        chmod($image, 0640);
        return $image;
    }

    public function onUpload(PostPersistEvent $event)
    {
        $type=$event->getType();
        
        switch($type) {
            case "avatar":
                $file=$event->getFile();
                $filename=$file->getFilename();
                $response = $event->getResponse();
                $response['file'] = $filename;
            break;

            case "hero":
                $file=$event->getFile();
                $filename=$file->getFilename();
                $response = $event->getResponse();
                $response['file'] = $filename;
            break;

            case "image":
                $file=$event->getFile();
                $filename=$file->getFilename();
                $response = $event->getResponse();
                $response['file'] = $filename;
            break;

            case "logo":
                $file=$event->getFile();
                $filename=$file->getFilename();
                $response = $event->getResponse();
                $response['file'] = $filename;
            break;   
            
            case "illustration":
                $file=$event->getFile();
                $request=$event->getRequest();
                $originalname=$request->files->all()["file"]->getClientOriginalName();
                $originalname=substr($originalname, 0, stripos($originalname, '.'));

                $filename=$file->getFilename();
                $width = $this->getWidth('uploads/illustration/'.$filename);
                $height = $this->getHeight('uploads/illustration/'.$filename);

                $fs = new Filesystem();

                // Création des miniatures proportionnelle
                $fs->copy('uploads/illustration/'.$filename,'uploads/illustration/thumbori_'.$filename);
                $max_width=500;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/illustration/thumbori_'.$filename,$width,$height,$scale);

                // Création des miniatures redimentionnable en carré
                $fs->copy('uploads/illustration/'.$filename,'uploads/illustration/thumb_'.$filename);
                $max_width=500;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/illustration/thumb_'.$filename,$width,$height,$scale);

                // Redimentionner si trop gros
                $max_width=1200;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/illustration/'.$filename,$width,$height,$scale);

                // Récupérer la dernière valeur de dimension
                $width = $this->getWidth('uploads/illustration/'.$filename);
                $height = $this->getHeight('uploads/illustration/'.$filename);

                $response = $event->getResponse();
                $response['file'] = $filename;
                $response['width'] = $width;
                $response['height'] = $height;
                $response['originalname'] = $originalname;
            break;                

            case "webzine":
                $file=$event->getFile();
                $filename=$file->getFilename();
                $width = $this->getWidth('uploads/webzine/'.$filename);
                $height = $this->getHeight('uploads/webzine/'.$filename);

                $fs = new Filesystem();

                // Création des miniatures proportionnelle
                $fs->copy('uploads/webzine/'.$filename,'uploads/webzine/thumbori_'.$filename);
                $max_width=350;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/webzine/thumbori_'.$filename,$width,$height,$scale);

                // Création des miniatures en carré
                $fs->copy('uploads/webzine/'.$filename,'uploads/webzine/thumb_'.$filename);
                $max_width=350;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/webzine/thumb_'.$filename,$width,$height,$scale);

                // Redimentionner si trop gros
                $max_width=1200;
                $scale = $max_width/$width;
                $this->resizeImage('uploads/webzine/'.$filename,$width,$height,$scale);

                // Récupérer la dernière valeur de dimension
                $width = $this->getWidth('uploads/webzine/'.$filename);
                $height = $this->getHeight('uploads/webzine/'.$filename);

                $response = $event->getResponse();
                $response['file'] = $filename;
                $response['width'] = $width;
                $response['height'] = $height;
            break;

        }
    }
}
