<?php 
namespace App\Service;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class passwordEncoder implements PasswordEncoderInterface
{

    public function encodePassword($raw, $salt)
    {
        $hash = "{SSHA}" . base64_encode(pack("H*", sha1($raw . $salt)) . $salt);
        return $hash;
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }

    public function needsRehash(string $encoded): bool
    {
        // check whether the current password is hash using an outdated encoder
        $hashIsOutdated = false;
        return $hashIsOutdated;
    }

}