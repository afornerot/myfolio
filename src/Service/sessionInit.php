<?php
    namespace App\Service;

    use Symfony\Component\DependencyInjection\ContainerInterface;
    use Symfony\Component\HttpKernel\Event\RequestEvent;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Doctrine\ORM\EntityManager;

    class sessionInit {
        private $container;
        protected $em;
        protected $session;

        public function __construct(ContainerInterface $container, EntityManager $em,Session $session)
        {
            $this->container = $container;
            $this->session = $session;
            $this->em = $em;
        }

        public function onDomainParse(RequestEvent $event) {
            $configs = $this->em->getRepository("App:Config")->findAll();
            $havehero=false;
            foreach($configs as $config) {
                if($config->getCategory()=="hero" && $config->getValue()!="") $havehero=true;
                $this->session->set($config->getId(), strval($config->getValue()));
            }

            // Valeur par défaut appname
            if($this->session->get("appname")=="")
                $this->session->set("appname", $this->container->getParameter('appName'));            

            // Valeur par defaut hero
            if(!$havehero) $this->session->set("hero01", "hero.jpg");

            // Valeur par defaut logo
            if($this->session->get("logodark")==""&&$this->session->get("logolight")=="") {
                $this->session->set("logodark", "logo.png");
                $this->session->set("logolight", "logo.png");
            }
            elseif($this->session->get("logodark")=="")
                $this->session->set("logodark", $this->session->get("logolight"));
            elseif($this->session->get("logolight")=="")
                $this->session->set("logolight", $this->session->get("logodark"));

            // Valeur par défaut imgcontact
            if($this->session->get("imgcontact")=="")
                $this->session->set("imgcontact", "contact.jpg");  

            // Valeur par défaut imglink
            if($this->session->get("imglink")=="")
                $this->session->set("imglink", "link.jpg");  

            // Calcul des couleurs
            /*
            $color = $this->container->get('cadoles.core.service.color');
            $color->setColor();
            */
        }
    }
