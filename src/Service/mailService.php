<?php
 
namespace App\Service;
 
class mailService
{
    protected $mailer;
    protected $twig;
 
    public function __construct(\Swift_Mailer $mailer, \Twig\Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }
 
    /**
     * Send email
     *
     * @param   string   $template      email template
     * @param   mixed    $parameters    custom params for template
     * @param   string   $to            to email address or array of email addresses
     * @param   string   $from          from email address
     * @param   string   $fromName      from name
     *
     * @return  boolean                 send status
     */
    public function sendEmail($subject, $body, $to, $from)
    {
        $template = $this->twig->load('Home/mail.html.twig');
 
        $parameters=["subject"=>$subject,"body"=>$body];
        $subject  = $template->renderBlock('subject', $parameters);
        $bodyHtml = $template->renderBlock('body', $parameters);
 
        try {
            $message = (new \Swift_Message())
                    ->setFrom('send@example.com')
                    ->setSubject($subject)
                    ->setFrom($from)
                    ->setTo($to)
                    ->setBody($bodyHtml, 'text/html');

            $response = $this->mailer->send($message);
 
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        return $response;
    }
}