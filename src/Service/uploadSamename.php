<?php

namespace App\Service;

use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;

class uploadSamename implements NamerInterface
{
    public function name(FileInterface $file)
    {
        return $file->getClientOriginalName();
    }
}