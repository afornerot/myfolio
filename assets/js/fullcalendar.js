// Fullcalendar
require('@fullcalendar/core/main.css');
require('@fullcalendar/daygrid/main.css');
require('@fullcalendar/daygrid/main.css');

import { Calendar } from '@fullcalendar/core';
import frLocale from '@fullcalendar/core/locales/fr.js';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';

var calendar;
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('fullcalendar');
    calendar = new Calendar(calendarEl, {
        plugins: [ interactionPlugin, dayGridPlugin ],
        locale: frLocale,
        weekNumbers: true,
        selectable: true,
        events: 'event/load',
        eventLimit:4, 
        eventDrop: function(info) {
            info.revert();
        },        
        eventRender: function (info) {
            eventRender(info);
        },
        select: function(selectionInfo) {
            eventSelect(selectionInfo);
        },
        eventClick: function(info) {
            eventClick(info);
        }
    });
    global.calendar = calendar;
    calendar.render();
});  
