// JQuery
window.$ = window.jQuery = require('jquery');

// JQueryui
require('jqueryui');

// Bootstrap
require('bootstrap');
require('bootstrap/dist/css/bootstrap.min.css');

// timepicki
const timepicki = require('timepicki/js/timepicki.js');

// Datatables
require('datatables.net-bs4');
require('./datatables.init.js');
require('datatables.net-bs4/css/dataTables.bootstrap4.min.css');

// Fontawesome
require('@fortawesome/fontawesome-free/css/all.css');

// bs-stepper
require('bs-stepper/dist/css/bs-stepper.min.css');
const Stepper = require('bs-stepper/dist/js/bs-stepper.min.js');
global.Stepper = Stepper;

// Masonery
const masonry = require('masonry-layout/dist/masonry.pkgd.min.js');
global.masonry = masonry;

// Slick
require('slick-carousel/slick/slick.css');
const slick = require('slick-carousel/slick/slick.min.js');
global.slick = slick;

// Imageloaded
const imagesLoaded = require('imagesloaded/imagesloaded.js');
global.imagesLoaded = imagesLoaded;

// Moment
const moment = require('moment');
global.moment = moment;
require('moment/locale/fr.js');

// Select2
require('select2');
require('select2/dist/js/i18n/fr.js');
require('select2/dist/css/select2.css');
require('@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css');
$(() => {
    $('.select2entity').select2(
        {
            theme: 'bootstrap4',
            language: "fr"
        }
    );
});

// CropSelectJs
require('crop-select-js/crop-select-js.min.css');
window.CropSelectJs = require('crop-select-js/crop-select-js.min.js');

// spectrum-colorpicker2
require('spectrum-colorpicker2/dist/spectrum.min.js');
require('spectrum-colorpicker2/dist/spectrum.min.css');


