#!/bin/bash

# Se positionner sur la racine du projet
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${DIR}
cd ..
DIR=$(pwd)

# Import des env
. $DIR/.env
. $DIR/.env.local

# Déclaration d'un proxy
if [[ "$PROXY_USE"=="1" ]]
then
    export HTTP_PROXY="$PROXY_HOST:$PROXY_PORT"
    export HTTPS_PROXY="$PROXY_HOST:$PROXY_PORT"
fi

# Suppression des logs trop ancien
if [[ -d $DIR/var/log ]]
then
find $DIR/var/log -mindepth 1 -mtime +7 -delete
fi

# Replace de l'alias dans le entrypoint
sed -i 's/\/myfolio\//\/'$APP_ALIAS'\//g' $DIR/public/build/*.*

# Installation des dépendances composer
echo COMPOSER = Install
composer install --quiet

php bin/console app:AppInit --env=prod
php bin/console app:CronInit --env=prod
php bin/console app:Script --env=prod

# Permissions
echo PERMISSIONS
./scripts/perm.sh www-data

echo 