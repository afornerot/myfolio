#!/bin/bash

# Se positionner sur la racine du projet
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${DIR}
cd ..
DIR=$(pwd)

# Définir le group propriétaire
group=$2
if [ -z $group ]
then
	group=$1
fi

# Placer les permissions
sudo chown $1:$group ${DIR} -R
sudo chmod +w ${DIR} -R
sudo chmod g+rw ${DIR} -R
