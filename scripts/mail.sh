#!/bin/bash

# Se positionner sur la racine du projet
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${DIR}
cd ..
DIR=$(pwd)

# Envoyer les mail
php bin/console swiftmailer:spool:send --message-limit=100 --time-limit=10 --transport app.sendmail.transport